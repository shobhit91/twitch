<html>
	<head>
		<title>Twitch API Implementation</title>
	</head>
<body>

<?php

include('config.php');

/**
*Get site URL
*/
function url(){
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'].'/clients/twitch';
}

//site url 
$siteURL = url();

session_start();
if (isset($_GET['code'])){
    $token_url = 'https://api.twitch.tv/kraken/oauth2/token';
    $data = array(
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'grant_type' => 'authorization_code',
        'redirect_uri' => $redirect_uri,
        'code' => $_GET['code'],
		'scope'=>'user_read'
    );

    $curl = curl_init($token_url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    $i = curl_getinfo($curl);	
    curl_close($curl);


    if ($i['http_code'] == 200) {
        $result = json_decode($result, true);
		
		//store user token in session for future use
		$_SESSION['user_token'] = $result['access_token'];

        // get user details
        $curl = curl_init('https://api.twitch.tv/kraken/user');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/vnd.twitchtv.v3+json',
            'Client-ID: ' . $client_id,
            'Authorization: OAuth ' . $result['access_token']
        ));
        $user = curl_exec($curl);
        $i = curl_getinfo($curl);
        curl_close($curl);

        if ($i['http_code'] == 200) {
            $user = json_decode($user);

            echo '<div class="container"><p>Hello ' . ucfirst($user->display_name) . ',</p><form name="fav_streamer" action="'.$siteURL.'/getDataByUser.php" method="POST"><input type="text" placeholder="Enter favourite streamer name" name="fav_streamer_name" required><input type="submit" name="fav_submit" value="Get details"></div>';

        } else {
            echo "<p>Oops, an error occured, please <a href='$siteURL'>click here</a></p>";
        }
    } else {
        echo "<p>Oops, an error occured, please <a href='$siteURL'>click here</a></p>";
    }
} else {
    $userScopes = array(
        'user_read' => 1,
    );

    $req_scope = '';
    foreach ($userScopes as $scope => $allow) {
        if ($allow) {
            $req_scope .= $scope . '+';
        }
    }
    $req_scope = substr($req_scope, 0, -1);
    $oauth_url = 'https://api.twitch.tv/kraken/oauth2/authorize?response_type=code';
    $oauth_url .= '&client_id=' . $client_id;
    $oauth_url .= '&redirect_uri=' . $redirect_uri;
    //$oauth_url .= '&scope=user_read';
    $oauth_url .= '&scope=' . $req_scope;
    $oauth_url .= '&force_verify=true';

    echo '<a href="' . $oauth_url . '">Login with Twitch</a>';
}
