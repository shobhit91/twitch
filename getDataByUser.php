<html>
	<head>
		<title>Streamer detailed page</title>
	</head>
<body>
<?php
include('config.php');

function file_get_contents_curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, 'Client-ID:'.$client_id);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

/*
*Get fav streamer data
*/
function getAllStreams($streamer_name, $client_id){

	$streamArray = json_decode(@file_get_contents("https://api.twitch.tv/kraken/channels/$streamer_name/videos?broadcasts=true&limit=10&client_id=$client_id"), true); 
	if ( $streamArray != null ) {
		echo '<div class="all_stream_container">';
		foreach($streamArray['videos'] as $mydata){
			if($mydata['title'] != null) {
				$broadcastId = $mydata['_id'];
				$title = ucfirst($mydata['title']);
				echo '<div class="box" style="width: 550px;float: left;margin-bottom: 20px;"><h3>'.$title.'</h3><br/><iframe src="http://player.twitch.tv/?video=' . $broadcastId . '" frameborder="0" scrolling="no" height="378" width="500" style="margin-bottom:20px; float:left"></iframe></div>';
			}
			else {
				echo "Oops, an error occured in response form Twitch API 1";  
			}
		}
		echo '<div>';
	}
	else {
		echo "Oops, an error occured in response form Twitch API 2";  
	}
}

$fav_name = $_POST['fav_streamer_name'] ? $_POST['fav_streamer_name'] : 'gtamarathon';

$channels = array("$fav_name","aimzatchu", "kittyplaysgames", "schyax", "cutenaomi", "cincinbear", "brialeigh", "juliehaze", "pallyberry"); 

$callAPI = implode(",",$channels);

$dataArray = json_decode(@file_get_contents('https://api.twitch.tv/kraken/streams?channel=' . $callAPI.'&client_id='.$client_id. '&limit=1'), true); 

$max_viewers=0;

$name='ERROR occured';

if ($dataArray['streams'] != null) {
    foreach($dataArray['streams'] as $mydata){
        if($mydata['_id'] != null){ 
			$streamer_id = $mydata['_id'];
            $viewers = $mydata['viewers'];
            if ( $viewers > $max_viewers ) { 
                $max_viewers = $viewers;  
                $name = $mydata['channel']['display_name'];
			}
        }
		if(session_id() == '') {
			session_start();
		}
    }
}
else {
    $backupArray = json_decode(@file_get_contents('https://api.twitch.tv/kraken/streams?limit=1' . $callAPI), true); 
    if ( $backupArray != null ) {
        foreach($backupArray['streams'] as $mydata){
            if($mydata['_id'] != null) {
                $name = $mydata['channel']['display_name'];
                $viewers = $mydata['viewers'];
            }
            else {
                echo "Oops, an error occured in response form Twitch API 3";  
            }
        }
    }
    else {
        echo "Oops, an error occured in response form Twitch API 4";  
    }
}

?>
<center>
	<h1> Streamer: <?php echo "$name" ?> </h1>
	<iframe src="https://embed.twitch.tv/?channel=<?php echo $name ?>" width="1200" height="600"></iframe>
	<div id="twitch-embed"></div>
	<script src="https://embed.twitch.tv/embed/v1.js"></script>

	<br/><h1> 10 most recent events</h1>
	<?php getAllStreams($name, $client_id);?>
</center>
</body>
</html>
