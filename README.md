# twitch

Demo URL  - http://urdev.tech/clients/twitch/

Note : As of now, in this implementation the RETURN URL is configured with my testing server, if you want to test on server then you just need to change the API Keys and Secret key in config.php then it will work properly.

-> How would you deploy the above on AWS? (ideally a rough architecture diagram will help)
There are several ways to Deploy code on AWS like via git/beanstalk or SFTP. In my architecture added in this code following a way with GitHub. Firstly a developer will commit and then push it to git. Then with git, a server is connected via SFTP and deploying code to AWS server.

Edit -

We usually make 3 servers i.e. Development, Testing and Production. So a developer work and run unit testing test case on Development server and then deploy code on Testing server for Quality Analyst(QA) for automation and regresion testing and once code get passed then deploy the code to production server.


-> Where do you see bottlenecks in your proposed architecture and how would you approach scaling this app starting from 100 reqs/day to 900MM reqs/day over 6 months?
In my architecture bottlenecks comes when there is server with low capacities like with smaller size of RAM and bandwidth, to overcome this  problem we can use auto-scaling on server then it will launch another instance when there is traffic and speed up the result set and when it there is less traffic then it will automatically terminate the instance.

Moreover, we can use microservices like Lamnda, API gateway and function applications then traffic will get divide to multiple resources and it will not choke the server. 